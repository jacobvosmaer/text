package main

import (
	"bufio"
	"io"
	"log"
	"os"

	"jacobvosmaer.nl/go/text"
)

func main() {
	if err := indent(os.Stdout, os.Stdin); err != nil {
		log.Fatal(err)
	}
}

func indent(_w io.Writer, r io.Reader) error {
	s := bufio.NewScanner(r)
	s.Split(text.ScanLinesRaw)

	w := bufio.NewWriter(_w)

	for s.Scan() {
		if _, err := w.WriteString("  "); err != nil {
			return err
		}
		if _, err := w.Write(s.Bytes()); err != nil {
			return err
		}
	}
	if err := w.Flush(); err != nil {
		return err
	}

	return s.Err()
}
