package main

import (
	"bufio"
	"bytes"
	"io"
	"log"
	"os"

	"jacobvosmaer.nl/go/text"
)

func main() {
	if err := undent(os.Stdout, os.Stdin); err != nil {
		log.Fatal(err)
	}
}

func undent(_w io.Writer, r io.Reader) error {
	s := bufio.NewScanner(r)
	s.Split(text.ScanLinesRaw)

	w := bufio.NewWriter(_w)

	for s.Scan() {
		if _, err := w.Write(bytes.TrimPrefix(s.Bytes(), []byte("  "))); err != nil {
			return err
		}
	}

	if err := w.Flush(); err != nil {
		return err
	}

	return s.Err()
}
