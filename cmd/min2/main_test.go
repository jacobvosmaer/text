package main

import (
	"io/ioutil"
	"strings"
	"testing"
)

func BenchmarkUndent(b *testing.B) {
	in := strings.Repeat(
		strings.Repeat("z", 120)+"\n  "+strings.Repeat("y", 120)+"\n",
		50000,
	)
	b.Run("bench", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			if err := undent(ioutil.Discard, strings.NewReader(in)); err != nil {
				b.Fatal(err)
			}
		}
	})
}
