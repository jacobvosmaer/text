package main

import (
	"bufio"
	"bytes"
	"errors"
	"io"
	"log"
	"os"
)

func main() {
	r := bufio.NewReader(os.Stdin)
	out, err := parse(r, 0)
	if err != nil {
		log.Fatal(err)
	}

	os.Stdout.Write(out)
}

func parseComment(r *bufio.Reader) ([]byte, error) {
	out := []byte(";")
	comment, err := r.ReadBytes('\n')
	if err != nil {
		return nil, err
	}
	out = append(out, comment...)
	return out, nil
}

func parseString(r *bufio.Reader) ([]byte, error) {
	out := []byte(`"`)
	for {
		buf, err := r.ReadBytes('"')
		if err != nil {
			return nil, err
		}
		out = append(out, buf...)
		if len(buf) == 1 || buf[len(buf)-2] != '\\' {
			break
		}
	}
	return out, nil
}

func parse(r *bufio.Reader, n int) ([]byte, error) {
	var out []byte

	for {
		c, err := r.ReadByte()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		switch c {
		case ';':
			buf, err := parseComment(r)
			if err != nil {
				return nil, err
			}
			out = append(out, buf...)
		case '(':
			buf, err := parse(r, n+1)
			if err != nil {
				return nil, err
			}
			c, err := r.ReadByte()
			if err != nil {
				return nil, err
			}
			if c != ')' {
				return nil, errors.New("missing closing parenthesis")
			}
			out = append(out, '(')
			out = append(out, bytes.TrimRight(buf, " ")...)
			out = append(out, ')')
		case ')':
			if n == 0 {
				return nil, errors.New("unexpected trailing parenthesis")
			}

			return out, r.UnreadByte()
		case '"':
			buf, err := parseString(r)
			if err != nil {
				return nil, err
			}
			if len(out) > 0 && last(out) != '(' && last(out) != ' ' {
				out = append(out, ' ')
			}
			out = append(out, buf...)
			if p, err := r.Peek(1); err == nil && p[0] != ')' && p[0] != ' ' {
				out = append(out, ' ')
			}
		case ' ', '\t':
			if len(out) == 0 {
				continue
			}
			if last(out) == ' ' || last(out) == '\n' {
				continue
			}

			out = append(out, ' ')
		case '\n':
			out = bytes.TrimRight(out, " ")
			out = append(out, '\n')
			out = append(out, bytes.Repeat([]byte("    "), n)...)
			continue
		default:
			if len(out) > 0 && last(out) == ')' {
				out = append(out, ' ')
			}
			out = append(out, c)
		}

	}

	if len(out) > 0 && last(out) != '\n' {
		out = append(out, '\n')
	}

	return out, nil
}

func last(b []byte) byte { return b[len(b)-1] }
