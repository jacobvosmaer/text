package text // import "jacobvosmaer.nl/go/text"

import (
	"bytes"
)

// ScanLinesRaw splits after each '\n' in the input. Unlike
// bufio.ScanLines this lets you detect input that has no trailing
// newline.
func ScanLinesRaw(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	if i := bytes.IndexByte(data, '\n') + 1; i >= 1 {
		return i, data[0:i], nil
	}
	// If we're at EOF, we have a final, non-terminated line. Return it.
	if atEOF {
		return len(data), data, nil
	}
	// Request more data.
	return 0, nil, nil
}
